Cara install
1.$ git clone https://gitlab.com/Cokoyomonoto/Password-generato
2.$ cd Password-generato
3.$ python 

Untuk mencegah agar sandi Anda diretas oleh manipulasi psikologis, kekerasan, atau metode serangan kamus, dan menjaga akun daring tetap aman, Anda harus memperhatikan bahwa:

1. Jangan gunakan kata sandi yang sama, pertanyaan keamanan dan jawaban untuk beberapa akun penting.

2. Gunakan kata sandi yang memiliki setidaknya 16 karakter, gunakan setidaknya satu nomor, satu huruf besar, satu huruf kecil dan satu simbol khusus

3. Jangan gunakan nama keluarga, teman atau hewan peliharaan Anda dalam kata sandi Anda.

4. Jangan gunakan kode pos, nomor rumah, nomor telepon, tanggal lahir, nomor KTP, nomor jaminan sosial, dan sebagainya di kata sandi Anda.

5. Jangan gunakan kata kamus apa pun dalam kata sandi Anda. Contoh kata sandi yang kuat: ePYHc ~ dS *) 8 $ + V- ', qzRtC {6rXN3N \ RgL, zbfUMZPE6`FC%) sZ. Contoh sandi yang lemah: qwert12345, Gbt3fC79ZmMEFUFJ, 1234567890, 987654321, nortonpassword.

6. Jangan menggunakan dua atau lebih kata sandi serupa yang sebagian besar karakternya sama, misalnya, ilovefreshflowersMac, ilovefreshflowersDropBox, karena jika salah satu dari kata sandi ini dicuri, maka itu berarti semua kata sandi ini dicuri.

7. Jangan menggunakan sesuatu yang dapat dikloning (tetapi Anda tidak dapat mengubah) sebagai kata sandi Anda, seperti sidik jari Anda.

8. Jangan biarkan browser Web Anda (FireFox, Chrome, Safari, Opera, IE) untuk menyimpan kata sandi Anda, karena semua kata sandi yang disimpan di browser Web dapat diungkap dengan mudah.

9. Jangan masuk ke akun penting di komputer orang lain, atau ketika terhubung ke hotspot Wi-Fi publik, Tor, VPN gratis atau proxy web.

10. Jangan mengirim informasi sensitif secara online melalui koneksi tidak terenkripsi (mis. HTTP atau FTP), karena pesan dalam koneksi ini dapat diendus dengan sedikit usaha. Anda harus menggunakan koneksi terenkripsi seperti HTTPS, SFTP, FTPS, SMTPS, IPSec bila memungkinkan.

11. Saat bepergian, Anda dapat mengenkripsi koneksi Internet Anda sebelum meninggalkan laptop, tablet, ponsel, atau router Anda. Misalnya, Anda dapat mengatur VPN pribadi (dengan protokol MS-CHAP v2 atau yang lebih kuat) di server Anda sendiri (komputer rumah, server khusus atau VPS) dan menyambung ke sana. Sebagai alternatif, Anda dapat mengatur terowongan SSH terenkripsi antara router Anda dan komputer rumah Anda (atau server jauh Anda sendiri) dengan Putty dan menghubungkan program Anda (misalnya FireFox) ke Putty. Kemudian bahkan jika seseorang menangkap data Anda saat ditransmisikan antara perangkat Anda (misalnya laptop, iPhone, iPad) dan server Anda dengan packet sniffer, mereka tidak akan dapat mencuri data dan kata sandi Anda dari data streaming yang dienkripsi.

12. Seberapa amankah kata sandi saya? Mungkin Anda percaya bahwa kata sandi Anda sangat kuat, sulit untuk diretas. Tetapi jika peretas telah mencuri nama pengguna Anda dan nilai hash MD5 dari kata sandi Anda dari server perusahaan, dan tabel pelangi dari peretas berisi hash MD5 ini, maka kata sandi Anda akan cepat retak.

Untuk memeriksa kekuatan kata sandi Anda dan mengetahui apakah mereka berada di dalam tabel pelangi populer, Anda dapat mengonversi kata sandi Anda menjadi hash MD5 pada generator hash MD5, kemudian mendekripsi kata sandi Anda dengan mengirimkan hash ini ke layanan dekripsi MD5 online. Misalnya, kata sandi Anda adalah "0123456789A", menggunakan metode brute-force, mungkin diperlukan waktu hampir satu tahun untuk meretas kata sandi Anda, tetapi jika Anda mendekripsinya dengan mengirimkan hash MD5-nya (C8E7279CD035B23BB9C0F1F954DFF5B3) ke situs web dekripsi MD5, caranya lama yang dibutuhkan untuk memecahkannya? Anda dapat melakukan tes sendiri.

13. Disarankan untuk mengubah kata sandi Anda setiap 10 minggu.

14. Disarankan agar Anda mengingat beberapa kata sandi master, menyimpan kata sandi lain dalam file teks biasa dan mengenkripsi file ini dengan 7-Zip, GPG atau perangkat lunak enkripsi disk seperti BitLocker, atau mengelola kata sandi Anda dengan perangkat lunak manajemen sandi.

15. Enkripsikan dan cadangkan kata sandi Anda ke berbagai lokasi, kemudian jika Anda kehilangan akses ke komputer atau akun Anda, Anda dapat mengambil kembali kata sandi Anda dengan cepat.

16. Aktifkan otentikasi 2 langkah bila memungkinkan.

17. Jangan simpan kata sandi penting Anda di cloud.

18. Akses situs web penting (misalnya Paypal) dari bookmark secara langsung, jika tidak silakan periksa nama domainnya dengan hati-hati, ada baiknya untuk memeriksa popularitas situs web dengan Alexa toolbar untuk memastikan bahwa itu bukan situs phishing sebelum memasukkan kata sandi Anda.

19. Lindungi komputer Anda dengan firewall dan perangkat lunak antivirus, blokir semua koneksi masuk dan semua koneksi keluar yang tidak perlu dengan firewall. Unduh perangkat lunak hanya dari situs terkemuka, dan verifikasi MD5 / SHA1 / SHA256 checksum atau tanda tangan GPG dari paket instalasi bila memungkinkan.

20. Jaga sistem operasi (misalnya Windows 7, Windows 10, Mac OS X, iOS, Linux) dan browser Web (mis. FireFox, Chrome, IE, Microsoft Edge) perangkat Anda (misalnya, PC Windows, Mac PC, iPhone, iPad , Tablet Android) diperbarui dengan menginstal pembaruan keamanan terbaru.

21. Jika ada file penting di komputer Anda, dan itu dapat diakses oleh orang lain, periksa apakah ada perangkat keras keyloggers (misalnya sniffer keyboard nirkabel), perangkat lunak keyloggers dan kamera tersembunyi ketika Anda merasa perlu.

22. Jika ada router WIFI di rumah Anda, maka Anda bisa mengetahui kata sandi yang Anda ketikkan (di rumah tetangga Anda) dengan mendeteksi gerakan jari dan tangan Anda, karena sinyal WIFI yang mereka terima akan berubah ketika Anda memindahkan jari-jari Anda dan tangan. Anda dapat menggunakan keyboard di layar untuk mengetikkan kata sandi Anda dalam kasus seperti itu, akan lebih aman jika keyboard virtual ini (atau keyboard lunak) mengubah tata letak setiap waktu.

23. Kunci komputer dan ponsel Anda saat Anda meninggalkannya.

24. Enkripsi seluruh hard drive dengan LUKS atau alat-alat serupa sebelum meletakkan file penting di dalamnya, dan hancurkan hard drive perangkat lama Anda secara fisik jika diperlukan.

25. Akses situs web penting dalam mode pribadi atau penyamaran, atau gunakan satu browser Web untuk mengakses situs web penting, gunakan yang lain untuk mengakses situs lain. Atau akses situs web yang tidak penting dan instal perangkat lunak baru di dalam mesin virtual yang dibuat dengan VMware, VirtualBox atau Parallels.

26. Gunakan setidaknya 3 alamat email yang berbeda, gunakan yang pertama untuk menerima email dari situs penting dan Aplikasi, seperti Paypal dan Amazon, gunakan yang kedua untuk menerima email dari situs dan Aplikasi yang tidak penting, gunakan yang ketiga (dari yang berbeda penyedia email, seperti Outlook dan GMail) untuk menerima email pengaturan ulang kata sandi Anda saat yang pertama (misalnya Yahoo Mail) diretas.

27. Gunakan setidaknya 2 nomor telepon differnet, JANGAN memberi tahu orang lain nomor telepon yang Anda gunakan untuk menerima pesan teks dari kode verifikasi.

28. Jangan klik tautan dalam email atau pesan SMS, jangan mengatur ulang kata sandi Anda dengan mengkliknya, kecuali Anda tahu bahwa pesan-pesan ini tidak palsu.

29. Jangan beri tahu kata sandi Anda kepada siapa pun di email.

30. Ada kemungkinan bahwa salah satu perangkat lunak atau Aplikasi yang Anda unduh atau perbarui telah dimodifikasi oleh peretas, Anda dapat menghindari masalah ini dengan tidak menginstal perangkat lunak atau Aplikasi ini pada saat pertama kali, kecuali bahwa itu diterbitkan untuk memperbaiki lubang keamanan. Anda dapat menggunakan aplikasi berbasis Web sebagai gantinya, yang lebih aman dan portabel.

31. Hati-hati saat menggunakan alat tempel dan alat tangkapan layar daring, jangan biarkan mereka mengunggah kata sandi Anda ke cloud.

32. Jika Anda seorang webmaster, jangan menyimpan kata sandi pengguna, pertanyaan keamanan dan jawaban sebagai teks biasa dalam database, Anda harus menyimpan nilai-nilai hash (SHA1, SHA256 atau SHA512) yang diasinkan sebagai gantinya. Direkomendasikan untuk menghasilkan string garam acak unik untuk setiap pengguna. Selain itu, ada baiknya untuk mencatat informasi perangkat pengguna (mis. Versi OS, resolusi layar, dll.) Dan menyimpan nilai hash asinnya, lalu ketika dia mencoba masuk dengan kata sandi yang benar tetapi perangkatnya informasi TIDAK cocok dengan yang disimpan sebelumnya, biarkan pengguna ini memverifikasi identitasnya dengan memasukkan kode verifikasi lain yang dikirim melalui SMS atau email.

33. Jika Anda seorang pengembang perangkat lunak, Anda harus menerbitkan paket pembaruan yang ditandatangani dengan kunci pribadi menggunakan GnuPG, dan verifikasi tandatangannya dengan kunci publik yang diterbitkan sebelumnya.

34. Untuk menjaga keamanan bisnis online Anda, Anda harus mendaftarkan nama domain Anda sendiri, dan mengatur akun email dengan nama domain ini, maka Anda tidak akan kehilangan akun email Anda dan semua kontak Anda, karena Anda dapat meng-host email Anda server di mana saja, akun email Anda tidak dapat dinonaktifkan oleh penyedia email.

35. Jika situs belanja online hanya memungkinkan untuk melakukan pembayaran dengan kartu kredit, maka Anda harus menggunakan kartu kredit virtual sebagai gantinya.

36. Tutup browser web Anda ketika Anda meninggalkan komputer Anda, jika tidak, cookie dapat disadap dengan perangkat USB kecil dengan mudah, sehingga memungkinkan untuk melewati verifikasi dua langkah dan masuk ke akun Anda dengan cookie yang dicuri di komputer lain.

37. Ketidakpercayaan dan hapus sertifikat SSL yang buruk dari browser Web Anda, jika tidak, Anda TIDAK akan dapat memastikan kerahasiaan dan integritas koneksi HTTPS yang menggunakan sertifikat ini.

38. Enkripsikan seluruh partisi sistem, jika tidak silakan nonaktifkan fungsi pagefile dan hibernasi, karena itu mungkin untuk menemukan dokumen penting Anda dalam file pagefile.sys dan hiberfil.sys.

39. Untuk mencegah serangan login brute force ke server khusus Anda, server VPS atau server cloud, Anda dapat menginstal perangkat lunak deteksi intrusi dan pencegahan seperti LFD (Login Failure Daemon) atau Fail2Ban.